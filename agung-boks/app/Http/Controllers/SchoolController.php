<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helpers;
class SchoolController extends Controller
{
    
    public function index()
    {
        return view('pages.school');
    }

    public function store(Request $request)
    {
        $parameter = [
            "name" => $request->name
        ];
        $token = $request->cookie("api_token");
        $request = Helpers::RequestAPI($parameter,"school/add","POST",$token);
        $response = json_decode($request);
        return redirect('/master-school');
    }

    public function list(Request $request)
    {
        $token = $request->cookie("api_token");
        $request = Helpers::RequestAPI([],"school/list","POST",$token);
        dd($request);
    }

   

}
