<?php

namespace App\Http\Controllers;
use GuzzleHttp\Clients;
use Illuminate\Http\Request;
use App\Helpers\Helpers;
use Faker\Extension\Helper;
use Mockery\Generator\Parameter;


class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    
    public function login(Request $request)
    {
        if($request->cookie('api_token') == ""){
            $parameter = [
             "email" => $request->input("email"),
             "password" => $request->input("password")
            ];
             $request = Helpers::RequestAPI($parameter,"auth/login","POST");
             $response = json_decode($request);
             if($response->success)
             {
                 return redirect('/dashboard')->withCookie(cookie('api_token',$response->data->api_token,60));
                }
            }else{
                return redirect('/dashboard');
        }
    }
    public function register()
    {
        return view('auth.registrasi');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parameter =[
            "name" => $request->name,
            "email" => $request->email,
            "password" => $request->password 
        ];
        $request = Helpers::RequestAPI($parameter,"auth/register","POST");
        $response = json_decode($request);
        if($response->success)
        {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
