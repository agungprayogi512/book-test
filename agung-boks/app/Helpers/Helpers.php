<?php

namespace App\Helpers;

class Helpers
{
    static function RequestAPI($parameter,$endpoint,$method,$token = false)
    {
        ini_set('memory_limit', '2048M');
        ini_set("memory_limit", "-1");
        set_time_limit(0);
     
   
        $fullUrl = "http://localhost:8001/".$endpoint;	
        // if($method =="GET" || $method == "get")
        // {
        //     $fullUrl = 
        // }
        $curl = curl_init();         
        curl_setopt_array($curl, [
            CURLOPT_URL => $fullUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($parameter),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER=> 0,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
                'Authorization: Bearer '.$token
            ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if($err){return $err;}
        return  $response;
    }
}