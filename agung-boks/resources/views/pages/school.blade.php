@extends("./layouts/dashboard")
@section('content')
<div class="container mt-3 col-4">
    <div class="card">
        <div class="card-header">
            <h4>Tambah Sekolah</h4>
        </div>
        <div class="card-body">
            <form class="form" id="form-registrasi" method="POST" action="{{ route('schoolAdd') }}">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" aria-describedby="nama">
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary" id="add-school">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<form action="{{route('masterSchoolList')}}" method="POST">
<button class="btn btn-primary">Tampilkan</button>
</form>
<div class="container mt-2">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">nama</th>
                <th scope="col">Aksi</th>
            
            </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>
</div>


@endsection