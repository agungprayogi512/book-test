@extends("./layouts/app")

@section("content")
<div class="container mt-3 col-4">
    <div class="card">
        <div class="card-header">
            <h4>Login</h4>
        </div>
        <div class="card-body">
            <form class="form" method="post" action="{{route('login')}}">
                @csrf
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="{{route('register')}}">Registrasi</a>
                </div>
              
            </form>
        </div>
    </div>
</div>
<script>

</script>
@endsection