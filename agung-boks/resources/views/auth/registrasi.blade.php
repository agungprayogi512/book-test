@extends("./layouts/app")

@section("content")
<div class="container mt-3 col-4">
    <div class="card">
        <div class="card-header">
            <h4>Registrasi</h4>
        </div>
        <div class="card-body">
            <form class="form" id="form-registrasi" method="POST" action="{{ route('registerStore') }}">
            @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" aria-describedby="nama">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary" id="register">Simpan</button>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="">Login</a>
                </div>
              
            </form>
        </div>
    </div>
</div>
@endsection