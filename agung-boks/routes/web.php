<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SchoolController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/login",[AuthController::class,'index']);
Route::post("/login",[AuthController::class,'login'])->name('login');
Route::get("/dashboard",[DashboardController::class,'index'])->name('dashboard');
Route::get("/register",[AuthController::class,'register'])->name('register');
Route::post('/register',[AuthController::class,'store'])->name('registerStore');
Route::get("/master-school",[SchoolController::class,'index']);
Route::post("/master-school-add",[SchoolController::class,'store'])->name("schoolAdd");
Route::post("/master-school-list",[SchoolController::class,"list"])->name("masterSchoolList");