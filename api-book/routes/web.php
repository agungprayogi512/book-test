<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// Generate Application Key
$router->get("/key",'KeyController@generateKey');

$router->group(['prefix' => 'auth'], function () use ($router){
    $router->post('register','AuthController@register');
    $router->post('login','AuthController@login');
});

$router->group(['prefix' => 'book'], function() use ($router){
    $router->get('show/{id}','BooksController@show');
    $router->post('add','BooksController@store');
    $router->put('update/{id}','BooksController@update');
    $router->delete('remove/{id}','BooksController@destroy');
});

$router->group(['prefix' => 'school'], function() use ($router){
    $router->get('show/{id}','SchoolsController@show');
    $router->post('list','SchoolsController@list');
    $router->post('add','SchoolsController@store');
    $router->put('update/{id}','SchoolsController@update');
    $router->delete('remove/{id}','SchoolsController@destroy');
});

$router->group(['prefix' => 'bookschool'], function() use ($router){
    $router->get('show/{id}','BookSchoolsController@show');
    $router->post('add','BookSchoolsController@store');
});

    
