<?php

namespace App\Http\Controllers;
use App\Models\Book;
use Illuminate\Http\Request;
class BooksController extends Controller 
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $name = $request->input('name');
        $isExsist = Book::where("name" ,$name)->first();
        if($isExsist == ""){
            $add = Book::create([
                "name" => $name
            ]);
            if($add){
                return response()->json([
                    "success" => true,
                    "message" => "Berhasil menambahkan buku!",
                    "data" => $add
                ],201);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Gagal menambahkan buku!",
                    "data" => ''
                ],400);
            }
        }else{
            return response()->json([
                "success" => false,
                "message"  => 'Book Sudah Ada!',
                "data" =>''
            ],400);
        }
    }

    public function show($id)
    {
        $book = Book::find($id);
        if(!empty($book)){
            return response()->json([
                "success" => true,
                "message" =>"Buku berhasil ditemukan!",
                "data" => $book
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Buku tidak ditemukan!",
                "data" => ''
            ],404);
        }
    }

    public function update(Request $request,$id)
    {
        $name = $request->input('name');
        $book = Book::find($id)->update(["name" => $name]);
        if($book){
            return response()->json([
                "success" => true,
                "message" =>"Buku berhasil diubah!",
                "data" => $book
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Buku gagal diubah!",
                "data" => ''
            ],400);
        }
    }
    public function destroy($id){
        $book = Book::find($id);
        if($book){
            return response()->json([
                "success" => true,
                "message" =>"Buku berhasil dihapus!",
                "data" => $book
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" =>"Buku gagal dihapus!",
                "data" => ''
            ],400);
        }
    }

}
