<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
class KeyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function generateKey()
    {
        $key = Str::random(32);
        return response()->json(['key' =>$key ]);
    }

}
