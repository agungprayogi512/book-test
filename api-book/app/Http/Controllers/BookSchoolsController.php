<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class BookSchoolsController extends Controller 
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $book_id = $request->input('book_id');
        $school_id = $request->input('school_id');
        $schoolHaveBook= $this->schoolHaveBook($school_id);
         $refrenceBookSchool = $this->refrenceBookSchool($book_id,$school_id);
        if($schoolHaveBook < 3){
            if($refrenceBookSchool < 2){
                $add = DB::table('book_schools')->insert([
                    "book_id" => $book_id,
                    "school_id" => $school_id
                ]);
                if($add){
                    return response()->json([
                        "success" => true,
                        "message" => "Data berhasil disimpan !",
                        "data" => $add
                    ],201);
                }else{
                    return response()->json([
                        "success" => false,
                        "message" => "Data gagal disimpan !",
                        "data" => ''
                    ],400);
                }
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Buku melebihi limit !",
                    "data" => ''
                ],400);
            }
          
        }else{
            return response()->json([
                "success" => false,
                "message" => "Skolah mencapai limit buku!",
                "data" => ''
            ],400);
        }
       
       
    }

 

    public function show($id)
    {
        $book = DB::select("
        select s.id,s.name  from schools as s where s.id = $id 
        ");

        $books = DB::select("select * from book_schools where book_schools.school_id = $id");
  
        if($book){
            return response()->json([
                "success" => true,
                "message" =>"Buku berhasil ditemukan!",
                "data" => [
                    $book,
                    "books" => $books
                ]
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Buku tidak ditemukan!",
                "data" => ''
            ],404);
        }
    }

    // public function update(Request $request,$id)
    // {
    //     $book_id = $request->input('book_id');
    //     $school_id = $request->input('school_id');
    //     $schoolHaveBook= $this->schoolHaveBook($school_id);
    //      $refrenceBookSchool = $this->refrenceBookSchool($book_id,$school_id);
    //     if($schoolHaveBook < 3){
    //         if($refrenceBookSchool < 2){
    //             $update = DB::table('book_schools')->where("id",$id)->update([
    //                 "book_id" => $book_id,
    //                 "school_id" => $school_id
    //             ]);
    //             if($update){
    //                 return response()->json([
    //                     "success" => true,
    //                     "message" => "Update data berhasil !",
    //                     "data" => $update
    //                 ],201);
    //             }else{
    //                 return response()->json([
    //                     "success" => false,
    //                     "message" => "Update data fail !",
    //                     "data" => ''
    //                 ],400);
    //             }
    //         }else{
    //             return response()->json([
    //                 "success" => false,
    //                 "message" => "Buku melebihi limit !",
    //                 "data" => ''
    //             ],400);
    //         }
          
    //     }else{
    //         return response()->json([
    //             "success" => false,
    //             "message" => "Skolah mencapai limit buku!",
    //             "data" => ''
    //         ],400);
    //     }
    // }
    // public function destroy($bookId,$schoolId){
    //     $book = DB::delete("delete from book_schools where book_id =$bookId and school_id = $schoolId");
    //     if($book){
    //         return response()->json([
    //             "success" => true,
    //             "message" =>"Buku berhasil dihapus!",
    //             "data" => $book
    //         ],201);
    //     }else{
    //         return response()->json([
    //             "success" => false,
    //             "message" =>"Buku gagal dihapus!",
    //             "data" => ''
    //         ],400);
    //     }
    // }

    private function schoolHaveBook($id)
    {   
        return DB::table("book_schools")->where("school_id",$id)->get()->count();
    }

    private function refrenceBookSchool($id,$schoolId)
    {
        return DB::table("book_schools")->where("book_id",$id)->where("school_id",$schoolId)->get()->count();
    }


   

}
