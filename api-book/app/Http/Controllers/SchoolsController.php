<?php

namespace App\Http\Controllers;
use App\Models\School;
use Illuminate\Http\Request;
class SchoolsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $name = $request->input('name');
        $isExsist = School::where("name",$name)->first();
        if($isExsist == ""){
            $add = School::create([
                "name" => $name
            ]);
            if($add){
                return response()->json([
                    "success" => true,
                    "message" => "Berhasil menambahkan sekolah!",
                    "data" => $add
                ],201);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Gagal menambahkan sekolah!",
                    "data" => ''
                ],400);
            }
        }else{
            return response()->json([
                "success" => false,
                "message"  => 'Sekolah Sudah Ada!',
                "data" =>''
            ],400);
        }
    }

    public function show($id)
    {
        $book = School::find($id)->first();
        if($book){
            return response()->json([
                "success" => true,
                "message" =>"Sekolah berhasil ditemukan!",
                "data" => $book
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Sekolah tidak ditemukan!",
                "data" => ''
            ],404);
        }
    }

    public function update(Request $request,$id)
    {
        $name = $request->input('name');
        $school = School::find($id)->update(["name" => $name]);
        if($book){
            return response()->json([
                "success" => true,
                "message" =>"Sekolah berhasil diubah!",
                "data" => $school
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Sekolah gagal diubah!",
                "data" => ''
            ],400);
        }
    }
    public function destroy($id){
        $school = School::find($id);
        if($book){
            return response()->json([
                "success" => true,
                "message" =>"Sekolah berhasil dihapus!",
                "data" => $school
            ],201);
        }else{
            return response()->json([
                "success" => false,
                "message" =>"Sekolah gagal dihapus!",
                "data" => ''
            ],400);
        }
    }

    public function list()
    {
        $school = School::get();
        if($school){
            return response()->json([
                "success" => true,
                "message" =>"sEkolah ditemukan!",
                "data" => $school
            ],201);
        }else{
            return response()->json([
                "success" => true,
                "message" =>"Sekolah tidak ditemukan!",
                "data" => ""
            ],404);
        }
    }

}
